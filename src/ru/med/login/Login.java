package ru.med.login;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Login extends JavaPlugin implements Listener{
	
	private String site;
	private String serverName;
	private String projectName;
	
	private FileConfiguration conf;
	
	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
		this.getCommand("starterkit").setExecutor(new StarterKitCMD());

		conf = getConfig();
		site = conf.getString("site");
		serverName = conf.getString("server");
		projectName = conf.getString("project");
	}
	
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onJoin(PlayerJoinEvent event){
		
		if(!event.getPlayer().hasPlayedBefore()){
			GiveStarter.giveStarterKit(event.getPlayer().getName());
		}
		
		event.getPlayer().sendMessage(ChatColor.GREEN + "Добро пожаловать сервер " + ChatColor.WHITE +  serverName + ChatColor.GREEN + " проекта " + ChatColor.WHITE + projectName + ChatColor.GREEN + "!");
	    event.getPlayer().sendMessage(ChatColor.GREEN + "Наш сайт " + ChatColor.WHITE + site);
		
		Player[] players = Bukkit.getOnlinePlayers();
		for(Player player : players){
			if (player.hasPermission("medlogin.see")){
				player.sendMessage(ChatColor.GRAY+"[Игрок "+event.getPlayer().getName()+" вошел в игру. IP:" + event.getPlayer().getAddress().getHostName() + "]");
			}
		}
		event.setJoinMessage("");
	}
}
