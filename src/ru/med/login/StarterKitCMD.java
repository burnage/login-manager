package ru.med.login;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StarterKitCMD implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender arg0, Command arg1, String arg2,	String[] arg3) {
		arg2 = arg3[0];
		if (arg0.hasPermission("loginmanager.givekit")){
			GiveStarter.giveStarterKit(arg2);
			Player[] players = Bukkit.getOnlinePlayers();
			for(Player player : players){
				if (player.getName().equals(arg2)) {
					player.sendMessage(ChatColor.DARK_AQUA + arg0.getName() + ChatColor.YELLOW + " выдал Вам стартовый набор!");
				}
			}
			
			arg0.sendMessage(ChatColor.GREEN + "Набор игроку " + ChatColor.DARK_AQUA + arg2 + ChatColor.GREEN + "выдан успешно!");
		} else {
			arg0.sendMessage(ChatColor.DARK_RED + "У Вас нет прав!");
		}
		return true;
	}
}
